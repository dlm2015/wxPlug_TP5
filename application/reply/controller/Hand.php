<?php
namespace app\reply\controller;

use think\Controller;
use app\msg\model\WxMsg;
use app\msgReply\model\WxMsgreply;

class Hand extends Controller {
	// 传入视图的参数
	protected $data;
	
	// 输入的数据 array
	protected $in;
	/**
	 * 构造函数
	 * 获取配置、数据；
	 * 数据初始化
	 */
	public function __construct() {
		global $ecms_hashur;
		parent::__construct ();
		
		$ecms_hashur = isset ( $ecms_hashur ) ? $ecms_hashur : '';
		// 获取默认公众号aid，加入数据$r;
		$common = new \app\common\controller\Index ();
		$wx = $common->getDefaultWx ();
		if (! empty ( $wx ['errCode'] )) {
			$this->error ( '中止操作：' . $wx ['errMsg'] );
		}
		
		$this->data = [ 
				'title' => '手动回复',
				'version' => config ( 'version' ),
				'ecms_hashur' => $ecms_hashur,
				'form_error' => array (),
				'public' => url ( '/', '', false ),
				'wx' => $wx ['data'],
				'aid' => $wx ['data'] ['id'] 
		];
		$this->in = isset ( $_POST ) && count ( $_POST ) > 0 ? $_POST : $_GET;
	}
	/**
	 * index
	 * @method 主函数
	 *
	 * @return string 封面页
	 */
	public function index() {
		$data = $this->data;
		$in = $this->in;
		$WxMsg = new WxMsg ();
		$User = new \app\user\controller\Index ();
		if ($in ['type'] == 'msg') {
			$my_query ['type'] = 'msg';
			$data ['msg'] ['id'] = $in ['id'];
			$my_query ['id'] = $in ['id'];
			$res = $WxMsg->get ( $in ['id'] );
			if ($res) {
				$r = $User->getTheUser ( $res ['user_name'] );
				if ($r ['errCode']) {
					$data ['user'] = [ ];
				} else {
					$data ['user'] = $r ['data'];
				}
				$where ['user_name'] = $res->user_name;
			} else {
				$this->error ( '系统打了个盹，请稍后重试！' );
			}
		}
		$my_query ['ecms_hashur'] = $data ['ecms_hashur'] ['href'];
		$where ['aid'] = $this->data ['aid'];
		if (isset ( $in ['search'] ) && (! empty ( $in ['search'] ) || $in ['search'] === 0)) {
			$where ['content'] = [ 
					'like',
					'%' . $in ['search'] . '%' 
			];
			$data ['search'] = $in ['search'];
			$my_query ['search'] = $in ['search'];
		}
		// 获取数据
		
		$list = $WxMsg->where ( $where )->paginate ( 5, false, [ 
				'query' => $my_query,
				'path' => '' 
		] ) // 保持链接稳定，尤其是修改等操作后跳转至本函数时
;
		$WxMsgreply =new WxMsgreply(); //此处有修改
		foreach ( $list as $k => $v) {
			$res = $WxMsgreply->where ( [ 
					'aid' => $this->data['aid'],
					'msg_id'=>$v['id']
			] )->select ();
			if($res){
// 				$res=$res->toArray();
				$list[$k]->msgreply=$res;
			}
			
// 			print_r($res);
// 			echo "<br><br>";
		}
// 		exit;
		$data ['msg_type'] = 'text';
		$data ['reply_content'] = $this->fetch ( 'common@./replyContent', $data );
		$data ['page'] = $list->render ();
		$data ['list'] = $list;
		return $this->view ( './hand', $data );
	}
	
	public function action(){
		$this->error("开发中……");
		exit;
	}
	/**
	 * view
	 * @method 显示
	 * @param array $temp 模板路径
	 * @param array $data 数据
	 * @return string HTML代码
	 */
	private function view($temp, $data) {
		$head = $this->fetch ( 'common@./head', $data );
		$foot = $this->fetch ( 'common@./foot', $data );
		return $head . $this->fetch ( $temp, $data ) . $foot;
	}
}
